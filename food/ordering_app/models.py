from django.db import models
from django.db.models import Avg
from django.contrib.auth.models import User

# Create your models here.


class FoodItem(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    price = models.DecimalField(max_digits=8, decimal_places=2)
    category = models.CharField(max_length=100, default=None)
    picture = models.ImageField(
        upload_to='pics', default='default-food-image-large.png')

    def average_rating(self):
        return Ratings.objects.filter(food_item=self).aggregate(Avg('rating'))['rating__avg']

    def __str__(self):
        return self.name


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20)
    address = models.TextField()

    def __str__(self):
        return self.user.username


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    total_price = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Order by {}'.format(self.user)


class Order_Item(models.Model):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name='order_items')
    food_item = models.ForeignKey(FoodItem, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    item_price = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return '{} price: {} quantity: {}'.format(self.food_item.name, self.food_item.price, self.quantity)


class Ratings(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    food_item = models.ForeignKey(
        FoodItem, on_delete=models.CASCADE, related_name='ratings')
    rating = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Ratting by {self.user} for {self.food_item}:{self.rating}'
