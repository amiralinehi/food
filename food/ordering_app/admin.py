from django.contrib import admin
from .models import FoodItem, UserProfile, Order_Item, Order, Ratings

# Register your models here.

admin.site.register(FoodItem)
admin.site.register(UserProfile)
admin.site.register(Order_Item)
admin.site.register(Order)
admin.site.register(Ratings)
