from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models.query import QuerySet
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from decimal import Decimal
from django.urls import reverse
from django.views.generic import ListView, DetailView
from .models import FoodItem, Order_Item, Order, UserProfile, Ratings
from django.forms.models import model_to_dict
from django.db.models import Q
from ordering_app.forms import CustomUserCreationForm, CustomUserLoginForm, PaymentForm
from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from django.contrib.auth import login, logout
from django.contrib import messages
from django.core.mail import send_mail
from sms import send_sms
# Create your views here.


class Menu(ListView):
    model = FoodItem
    context_object_name = 'foods'
    extra_context = {'maincourse': FoodItem.objects.filter(
        category='Main course'), 'appitizer': FoodItem.objects.filter(category='Appitizer'), 'drinks': FoodItem.objects.filter(category='Drink')}
    template_name = 'ordering_app/menu.html'


def home(request):
    return render(request, 'ordering_app/home.html')


class FoodDetails(DetailView):
    model = FoodItem
    context_object_name = 'food'
    template_name = 'ordering_app/food_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ratings'] = Ratings.objects.filter(
            food_item=self.object)  # Get ratings for the current food item
        return context


def get_cart(request):
    # Get the cart from the session, or create an empty cart
    cart = request.session.get('cart', {})
    return cart


def update_cart(request, cart):
    # Update the session with the cart data
    request.session['cart'] = cart
    request.session.modified = True  # Mark the session as modified


@login_required
def add_to_cart(request, id=None):
    cart = get_cart(request)
    if request.method == "POST":
        for food in FoodItem.objects.all():
            quantity = int(request.POST.get(f'quantity_{food.id}', 0))
            if quantity > 0:
                price = float(food.price)  # Convert Decimal to float

                if str(food.id) in cart:
                    cart[str(food.id)]['quantity'] += quantity
                else:
                    cart[str(food.id)] = {
                        'food_id': food.id,
                        'name': food.name,
                        'quantity': quantity,
                        'price': price  # Store price as float
                    }
        print(cart, 'none id')
        update_cart(request, cart)

    return redirect('menu')


@login_required
def create_order(request):
    user = request.user
    cart = get_cart(request)
    user_profile = UserProfile.objects.get(user=user)

    if request.method == "POST":
        item_quantities = {}

        order, created = Order.objects.get_or_create(
            user=user, defaults={'total_price': 0.0})

        for food_id, item_data in cart.items():
            quantity = item_data['quantity']
            price = item_data['price']
            food = FoodItem.objects.get(id=food_id)

            Order_Item.objects.create(
                order=order, food_item=food, quantity=quantity, item_price=price)

            if food_id in item_quantities:
                item_quantities[food_id]['quantity'] += quantity
            else:
                item_quantities[food_id] = {
                    'quantity': quantity, 'price': price}

        order.total_price = sum(item_data['quantity'] * item_data['price']
                                for item_data in item_quantities.values())
        order.user_profile = user_profile
        order.save()

        # Clear the cart after the order is created
        request.session['cart'] = {}
        request.session.modified = True

        return redirect('confirm-order')

    return HttpResponse("This is a GET request or another case.")


@login_required
def confirm_order(request):
    user = request.user
    try:
        # show latest items in query set
        user_order = Order.objects.get(user=user)
        user_info = UserProfile.objects.get(user=user)

        context = {
            'price': user_order.total_price,
            'user': user_info
        }

        print(user_order.total_price, user_info, ' from order_confirm')

        return render(request, 'ordering_app/order_confirm.html', context)
    except Order.DoesNotExist:
        return HttpResponse('Order not found for the current user')


@login_required
def cancel_order(request):
    if request.method == 'GET':
        user = request.user

        try:
            order = Order.objects.get(user=user)
            order.delete()
            return HttpResponseRedirect(reverse("menu"))

        except Order.DoesNotExist:
            return HttpResponse('order does not exist')

        except Exception as e:
            return HttpResponse(f'An error ocurred:{str(e)}')

    return HttpResponse("somthing went wrong when canceling order")


class SearchFood(ListView):
    model = FoodItem
    template_name = 'ordering_app/search_food.html'

    def get_queryset(self):
        querry = self.request.GET.get('q')
        object_list = FoodItem.objects.filter(
            Q(name__contains=querry) | Q(description__contains=querry) | Q(category__contains=querry))
        return object_list


def register(request):
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            try:
                profile = UserProfile.objects.get(user=user)
                print(profile)
            except UserProfile.DoesNotExist:
                print("UserProfile not created")
            UserProfile.objects.create(
                user=user, phone_number=form.cleaned_data['phone_number'], address=form.cleaned_data['address'])
            messages.success(
                request, f'Your account has been created! you can now login ', user)
            return redirect('home')
        else:
            print(form.errors)
    else:
        form = CustomUserCreationForm()
    return render(request, 'ordering_app/register.html', {'form': form})


# class CustomLoginView(LoginView):
#     template_name = 'ordering_app/login.html'  # Specify your login template
#     success_url = reverse_lazy('home')  # Redirect to the 'home' view upon successful login

def login_view(request):
    if request.method == "POST":
        form = CustomUserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            print(user, 'from login')
            return redirect('menu')
    else:
        form = CustomUserLoginForm()

    return render(request, 'ordering_app/login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('login')


def send_email(request):
    user = request.user
    phone_number = UserProfile.objects.get(user=user).phone_number
    email = user.email
    user_order = Order.objects.get(user=user)
    price = user_order.total_price
    # After everything is done, send confirmation email to the user
    body = (f'Thank you {user} for your order! Your food is being made and will be delivered soon!\n'
            f'Your total: {price}\n'
            'Thank you again for your order!')

    send_mail('Thank You For Your Order!', body,
              'test@gmail.com', [email], fail_silently=False)
    send_sms(
        'Thank You For Your Order!',
        '+09152735921',
        [phone_number],
        fail_silently=False
    )
    return redirect('menu')


def rate_food(request, pk):
    if request.method == 'POST':
        rating_value = int(request.POST.get('like'))
        food_item = FoodItem.objects.get(pk=pk)
        Ratings.objects.create(
            user=request.user, food_item=food_item, rating=rating_value)
    return HttpResponse('Rating submitted successfully')


def payment(request):
    form = PaymentForm()
    user = request.user
    order = Order.objects.get(user=user)
    return render(request, 'ordering_app/payment.html', context={'form': form,
                                                                 'price': order.total_price})
