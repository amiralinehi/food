from django.urls import path
from .views import Menu, FoodDetails, SearchFood
from . import views

urlpatterns = [

    path('menu/', Menu.as_view(), name='menu'),
    path('menu/<int:pk>', FoodDetails.as_view(), name='food-details'),
    path('search/', SearchFood.as_view(), name='food-search'),
    path('', views.home, name='home'),
    # path('add-to-cart/<int:id>/', views.add_to_cart, name='add-to-cart'),
    path('add-to-cart/', views.add_to_cart, name='add-to-cart-no-id'),
    path('create-order/', views.create_order, name='create-order'),
    path('confirm-order/', views.confirm_order, name='confirm-order'),
    path('cancel-order/', views.cancel_order, name='cancel-order'),
    path('send-email/', views.send_email, name='send-email'),
    path('food-rating/<int:pk>/', views.rate_food, name='food-rating'),
    path('payment/', views.payment, name='payment'),
    path('register/', views.register, name='register'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
]
