from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from ordering_app.models import UserProfile
from django.contrib.auth.models import User


class CustomUserCreationForm(UserCreationForm):

    email = forms.EmailField()
    phone_number = forms.CharField(max_length=20)
    address = forms.CharField(max_length=255)

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class CustomUserLoginForm(AuthenticationForm):

    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class PaymentForm(forms.Form):
    number = forms.CharField(max_length=16)
    cvv2 = forms.CharField(max_length=4)
    credit_password = forms.CharField(max_length=8)

    class Meta:
        fields = ['number', 'cvv2', 'credit_password']
